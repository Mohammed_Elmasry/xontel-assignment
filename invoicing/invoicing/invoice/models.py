from django.db import models
import datetime, xmlrpc.client
# Create your models here.

# define connection related constants
UID = 2
DB = "invoicing_system"
PASSWORD = "admin"
URL = "http://localhost:8069"
ODOO_CONNECTOR = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(URL))

class Invoice(models.Model):
    payment_plan = models.CharField(max_length=15, default="30 Net Days")
    odoo_record_number = models.IntegerField(unique=True, default=0, editable=False)

    def __str__(self):
        return "INV"+str(self.id) + "-" + datetime.datetime.today().strftime("%m/%d/%Y %H:%M")


    def save(self, *args, **kwargs):

        if (self.id): #then it's an update request
            ODOO_CONNECTOR.execute_kw(DB, UID, PASSWORD, 'account.invoice', 'write', [[self.odoo_record_number], {'partner_id':1, 'payment_term_id':3}])
        else:
            today = datetime.datetime.today().date().isoformat()
            self.odoo_record_number = ODOO_CONNECTOR.execute_kw(DB, UID, PASSWORD, 'account.invoice', 'create', [{'partner_id':1, 'date_invoice': today, 'payment_term_id':3 }])

        return super(Invoice, self).save(*args, **kwargs)


    def delete(self, *args, **kwargs):

        # Models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
        ODOO_CONNECTOR.execute_kw(DB, UID, PASSWORD, 'account.invoice', 'unlink', [[self.odoo_record_number]])
        super(Invoice, self).delete(*args, **kwargs)