from django.contrib import admin
from invoice.models import *

class InvoiceAdmin(admin.ModelAdmin):
    readonly_fields = (Invoice.payment_plan,)


class CustomerAdmin(admin.ModelAdmin):
    pass
admin.site.register(Customer)
admin.site.register(Invoice)